﻿namespace ShareModels.Models
{
    public class UserListRoleModel
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public string UserName { get; set; }
        public string ListKey { get; set; }
        public string Temp { get; set; }
    }
}
