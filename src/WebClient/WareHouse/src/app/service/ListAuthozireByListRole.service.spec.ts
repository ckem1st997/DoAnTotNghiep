/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ListAuthozireByListRoleService } from './ListAuthozireByListRole.service';

describe('Service: ListAuthozireByListRole', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListAuthozireByListRoleService]
    });
  });

  it('should ...', inject([ListAuthozireByListRoleService], (service: ListAuthozireByListRoleService) => {
    expect(service).toBeTruthy();
  }));
});
