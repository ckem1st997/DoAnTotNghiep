/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ListAuthozireRoleByUserService } from './ListAuthozireRoleByUser.service';

describe('Service: ListAuthozireRoleByUser', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListAuthozireRoleByUserService]
    });
  });

  it('should ...', inject([ListAuthozireRoleByUserService], (service: ListAuthozireRoleByUserService) => {
    expect(service).toBeTruthy();
  }));
});
